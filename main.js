"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
const gSIZE_S = "S";
const gSIZE_M = "M";
const gSIZE_L = "L";
const gPIZZA_OCEAN = "Ocean";
const gPIZZA_HAIWAI = "Haiwai";
const gPIZZA_BACON = "Bacon";
var gDataSizeS = "";
var gDataSizeM = "";
var gDataSizeL = "";
var gDataOcean = "";
var gDataHaiwai = "";
var gDataBacon = "";


/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onloadDataDrink();
    $("#btn-size-s").on("click", function () {
        onBtnSizeS();
        console.log(gDataSizeS);
    })
    $("#btn-size-m").on("click", function () {
        onBtnSizeM();
        console.log(gDataSizeM);
    })
    $("#btn-size-l").on("click", function () {
        onBtnSizeL();
        console.log(gDataSizeL);
    })
    $("#btn-ocean").on("click", function () {
        onBtnOcean();
        console.log(gDataOcean);
    })
    $("#btn-haiwai").on("click", function () {
        onBtnHaiwai();
        console.log(gDataHaiwai);
    })
    $("#btn-bacon").on("click", function () {
        onBtnBacon();
        console.log(gDataBacon);
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnSizeS() {
    changeColorCombo("S");
    gDataSizeS = getDataCombo("S", "20cm", "2", "2", "200g", 150000);
}
function onBtnSizeM() {
    changeColorCombo("M");
    gDataSizeM = getDataCombo("M", "25cm", "3", "4", "300g", 200000);
}
function onBtnSizeL() {
    changeColorCombo("L");
    gDataSizeL = getDataCombo("L", "30cm", "4", "8", "500g", 250000);
}
function onBtnOcean() {
    changeColorLoaiPizza(gPIZZA_OCEAN);
    gDataOcean = "Pizza Ocean";

}
function onBtnHaiwai() {
    changeColorLoaiPizza(gPIZZA_HAIWAI);
    gDataHaiwai = "Pizza Haiwai";
}
function onBtnBacon() {
    changeColorLoaiPizza(gPIZZA_BACON);
    gDataBacon = "Pizza Bacon";
}

function onloadDataDrink() {
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        success: function (paramRes) {
            getDataSelectDrink(paramRes);
            console.log(paramRes);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    })

}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getDataCombo(paramSize, paramDuongKinh, paramNuocNgot, paramSuon, paramSalad, paramVND) {
    var vObjCombo = {
        kichCo: paramSize,
        duongKinh: paramDuongKinh,
        soLuongNuoc: paramNuocNgot,
        suon: paramSuon,
        salad: paramSalad,
        thanhTien: paramVND
    }
    return vObjCombo;
}

function changeColorCombo(paramSize) {
    if (paramSize == "S") {
        $("#btn-size-s").data("data-combo-size", "Y")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");

    }
    else if (paramSize == "M") {
        $("#btn-size-s").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
    }
    else if (paramSize == "L") {
        $("#btn-size-s").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-m").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-size-l").data("data-combo-size", "N")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
    }

}
// loai pizza
function changeColorLoaiPizza(paramLoai) {
    if (paramLoai == gPIZZA_OCEAN) {
        $("#btn-ocean").data("data-loai-pizza", "Y")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-haiwai").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-bacon").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");

    }
    else if (paramLoai == gPIZZA_HAIWAI) {
        $("#btn-ocean").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-haiwai").data("data-loai-pizza", "Y")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
        $("#btn-bacon").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
    }
    else if (paramLoai == gPIZZA_BACON) {
        $("#btn-ocean").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-haiwai").data("data-loai-pizza", "N")
            .removeClass()
            .addClass("btn btn-warning col-sm-12");
        $("#btn-bacon").data("data-loai-pizza", "Y")
            .removeClass()
            .addClass("btn btn-success col-sm-12");
    }

}

function getDataSelectDrink(paramRes) {
    for (var bI = 0; bI < paramRes.length; bI++) {
        $("#select-drink").append($("<option>", {
            text: paramRes[bI].tenNuocUong,
            value: paramRes[bI].maNuocUong,
        }))
    }

}